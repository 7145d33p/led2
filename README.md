## Synopsis

This is a test project I used to test Ubuntu toolchain for Atmel SAMD21 MCU development. This includes gcc toolchain, header files for SAMD21 MCU, sample makefie and procedure to use gdb for remote debug as described in my article on 7145d33p.wordpress.com.

## Hardware

SAMD21 Xplained pro board + OLED1 Xplained pro board

[Updated Jan 2020:
 1. deleted OLED1 Xplained pro board dependence
 2. only blinks LED on SAMD21 Xplained pro board
 3. need to use samd21_asf library (see samd21_asf repo)
 4. modified link script and makefile to relocate code to 0x1000. The bin can be used
    directly by using D21DFU bootloader.

 To build,
```
$ make clean
$ make
```

 To download to SAMD21 Xplained pro board with D21DFU bootloader,
 1. connect PC to SAMD21 XPRO board's "TARGET USB" port (not the "DEBUG USB" port)
 2. push SW0 and RESET button at the same time
 2. release RESET button while holding SW0 button, you will see LED is blinking ~1.5sec.
 3. run the following command
```
$ sudo ./d21dfu -I03eb.2ff4 -p led_0x1000.bin
```
 You will that see the led blinks differently.

 See D21DFU bootloader repo for instruction to program bootloader into SAMD21 XPRO board using linux.

## Code

The code itself is a re-written version of Atmel's example in ASF without using ASF library. All the register access are directly done over pointers just like ordinary MCU way.

