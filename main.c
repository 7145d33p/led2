#include "sam.h"
#include "main.h"

void initsystem(void)
{
	uint32_t coarse, fine;
	
	//update all calibration parameters from NVM
	REG_ADC_CALIB = ((*(uint32_t*)(NVMCTRL_OTP4) >> 27) & 0x1f) | ((*(uint32_t*)(NVMCTRL_OTP4 + 4) << 5) & 0x7e0);	// LLLL L--- ---- ---- ---- ---- ---- ----
																													// ---- ---- ---- ---- ---- ---- --BB BLLL
	REG_SYSCTRL_OSC32K = (REG_SYSCTRL_OSC32K & 0xFF00FFFF) | ((*(uint32_t*)(NVMCTRL_OTP4 + 4) << 10) & 0x7f0000); 	// ---- ---- ---- ---- ---C CCCC CC-- ----
	REG_USB_PADCAL     = ((*(uint32_t*)(NVMCTRL_OTP4 + 4) >> 18) & 0x1f) | ((*(uint32_t*)(NVMCTRL_OTP4 + 4) >> 7) & (0x1f << 6)) | ((*(uint32_t*)(NVMCTRL_OTP4 + 4) >> 11) & (0x07 << 12));  // ---- --TT TPPP PPNN NNNC CCCC CC-- ----
	//=== flash wait state =========================
	REG_NVMCTRL_CTRLB = (2 << 1);   //2 flash wait state
	//=== 06092015: Config Clocks ============================
	// ULPOSC32 -> GCLKGEN1 --> DFLL/GCLKMUX0 -> DFLL -> GCLKGEN0 -> 48MHz mainck
	//                 |                                     `-----> SPI clock
	//                 `------> GCLKMUX1 ---> SERCOM slow clock (32kHz) 
	// OSC8M -->   GCLKGEN2 (%1) -> GCLKMUX2 ---> ADC CLK, SERCOM Core CLK (8MHz)
	REG_SYSCTRL_OSC8M &= ~SYSCTRL_OSC8M_PRESC (3);	//change prescaler to %1. i.e 8MHz
	REG_PM_APBCMASK |= PM_APBCMASK_SERCOM3 | PM_APBCMASK_SERCOM5 | PM_APBCMASK_ADC;	//enable SERCOM3 SERCOM5 and ADC clock
	//REG_GCLK_GENDIV  = 0;								//32kHz GCLK MUX 1
	REG_GCLK_GENDIV  = 1;								//32kHz GCLK MUX 1
	REG_GCLK_GENDIV  = 2;								//8MHz  GCLK MUX 2
	REG_GCLK_GENCTRL = 0x00010301;  					//OSCULP32 -> GCLKGEN1 32kHz
	REG_GCLK_GENCTRL = 0x00010602;						//OSC8M -> GCLKGEN2 = 8MHz
	REG_GCLK_CLKCTRL = 0x4100;							//GCLKGEN1 (1MHz)--> DFLLREF
	REG_SYSCTRL_DFLLCTRL = 0x0046; 						//write ONDEMAND=0 (errata)
	REG_SYSCTRL_DFLLMUL = (0x7 << 26) | (0x3f << 16) | (48000000 / 32768); // default step size; 48MHz (errata)
	//REG_SYSCTRL_DFLLMUL = (0x07 << 24) | (0x3f << 16) | 6; // default step size; 48MHz (errata)
	coarse = *(uint32_t*)(NVMCTRL_OTP4 + 4) >> 26;
	fine   = *(uint32_t*)(NVMCTRL_OTP4 + 8) & 0x3ff;
	if (coarse == 0x3f) coarse = 0x1f;
	if (fine == 0x3ff) fine = 0x1ff;
	REG_SYSCTRL_DFLLVAL =  (coarse << 10) | fine;
	while(!(REG_SYSCTRL_PCLKSR&(0x1<<4)) );
	REG_SYSCTRL_DFLLCTRL = 0x0046; 				//close loop mode, enable
	while (!(REG_SYSCTRL_PCLKSR & 0x10));					//wait for DFLL ready
	REG_GCLK_GENCTRL = 0x00010700;	//DFLL --> GCLKGEN0.
	
	__disable_irq();
	
	//--- LED and SW ----
    //REG_PORT_DIRSET1 = (1 << LED_PIN);    //PB30 as output
	
	//--- init sercom for UARTs ---
    REG_GCLK_CLKCTRL = 0x4217;      //no wr lock, CLK EN, CLK GEN2 (8MHz) --> SERCOM3_CORE CLK
    REG_GCLK_CLKCTRL = 0x4113;      //no wr lock, CLK EN, CLK GEN1 (32kHz) --> SERCOMx_SLOW CLK
	REG_PORT_DIRCLR0 = (1<<EDBG_UART_RX_PIN);        //set Rx pin to input
    REG_PORT_DIRSET0 = (1<<EDBG_UART_TX_PIN);        //set Tx pin to output
    REG_PORT_WRCONFIG0 = (PORT_WRCONFIG_HWSEL) | (1<<(EDBG_UART_RX_PIN-16)) | (1<<(EDBG_UART_TX_PIN-16)) | \
						 (PORT_WRCONFIG_WRPINCFG) | (PORT_WRCONFIG_INEN) | (PORT_WRCONFIG_WRPMUX) | (PORT_WRCONFIG_PMUXEN) | \
						 (PORT_PMUX_PMUXE_C_Val << PORT_WRCONFIG_PMUX_Pos);
    REG_SERCOM3_USART_CTRLB = SERCOM_USART_CTRLB_RXEN | SERCOM_USART_CTRLB_TXEN;        //enable TXEN, RXEN, 1 stop bit, 8 data bits
    REG_SERCOM3_USART_BAUD = 50436;               // (1-(50436/65536))*8MHz/16 = 115200
	REG_SERCOM3_USART_CTRLA = 0x40100086U;  //0x40310086U;
	//REG_SERCOM3_USART_CTRLA = SERCOM_USART_CTRLA_DORD | SERCOM_USART_CTRLA_RXPO(1) | SERCOM_USART_CTRLA_MODE_USART_INT_CLK | SERCOM_USART_CTRLA_ENABLE;
	
	//--- init sercom 5 for SPI ---
	REG_GCLK_CLKCTRL = GCLK_CLKCTRL_CLKEN | GCLK_CLKCTRL_GEN_GCLK0 | GCLK_CLKCTRL_ID_SERCOM5_CORE; //48MHz
	REG_PORT_WRCONFIG1 = (PORT_WRCONFIG_HWSEL) | (1<<(DISP_MOSI_PIN-16)) | (1<<(DISP_SCK_PIN-16)) | \
						 PORT_WRCONFIG_WRPINCFG | PORT_WRCONFIG_INEN | PORT_WRCONFIG_PMUXEN | PORT_WRCONFIG_WRPMUX | \
						 (PORT_PMUX_PMUXE_D_Val << PORT_WRCONFIG_PMUX_Pos);
	REG_PORT_DIRSET0 = (1<<DISP_LED1_PIN) | (1<<DISP_LED2_PIN) | (1<<DISP_LED3_PIN) | (1<<DISP_RST_PIN);		//3 LEDs on OLED board
	REG_PORT_DIRSET1 = (1<<DISP_DAT_CMD_PIN) | (1<<DISP_SS_PIN);
	REG_SERCOM5_SPI_BAUD = 0;
	REG_SERCOM5_SPI_CTRLA = SERCOM_SPI_CTRLA_DIPO(0) | SERCOM_SPI_CTRLA_DOPO(1) | SERCOM_SPI_CTRLA_MODE_SPI_MASTER;		//PAD2 as MISO, PAD0=MOSI, PAD1=SCK
	REG_SERCOM5_SPI_CTRLB = SERCOM_SPI_CTRLB_RXEN;
	REG_SERCOM5_SPI_CTRLA |= SERCOM_SPI_CTRLA_ENABLE;		//starts SPI0
	
	//--- init ADC ---------
	REG_GCLK_CLKCTRL = 0x421e;					//no wr lock, CLK EN, CLK GEN2 (8MHz) --> ADC CLK
	REG_ADC_REFCTRL = ADC_REFCTRL_REFCOMP | ADC_REFCTRL_REFSEL_INTVCC1;	//half of Vcc as reference
	//default REG_ADC_SAMPCTRL = ADC_SAMPCTRL(0);			//ADC CLK / 2
	//default REG_ADC_CTRLB = ADC_CTRLB_PRESCALER_DIV4;		//prescaler=DIV4, 12-bit, single conversion, singled-ended mode
	REG_ADC_INPUTCTRL = ADC_INPUTCTRL_GAIN_2X | ADC_INPUTCTRL_MUXNEG_GND | ADC_INPUTCTRL_MUXPOS_PIN7;	//2x gain. AIN07 PA07
	REG_PORT_WRCONFIG0 = (1<<AIN7_PIN) | PORT_WRCONFIG_INEN | PORT_WRCONFIG_PMUXEN | \
					     (PORT_WRCONFIG_WRPINCFG) | (PORT_PMUX_PMUXE_B_Val << PORT_WRCONFIG_PMUX_Pos); //PA07
	REG_ADC_CTRLA = ADC_CTRLA_ENABLE;

    //--- init GPIO LED ----
    REG_PORT_DIRSET1 = (1<<LED_PIN);        //set LED0 pin to output
}

//---------------------------------------------------------------
//--- main()
//---------------------------------------------------------------
int main(void)
{
	volatile int i;
	/* Initialize the SAM system. */
	initsystem();

	while (1) {
		for (i=0; i<3000000; i++) i=i;
        REG_PORT_OUTCLR1=(1<<LED_PIN);
		for (i=0; i<100000; i++) i=i;
        REG_PORT_OUTSET1=(1<<LED_PIN);
	}
}
