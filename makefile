TGT_NAME := led
TGT_C_SRCS := $(wildcard *.c)
TGT_C_SRCS += ./device_dep/startup_samd21.c
TGT_C_SRCS += ./device_dep/system_samd21.c

TGT_CXX_SRCS := $(wildcard *.cpp)
TGT_C_OBJS := ${TGT_C_SRCS:.c=.o} 
TGT_CXX_OBJS := ${TGT_CXX_SRCS:.cpp=.o} 
TGT_INC_DIRS := /home/tng/project02/samd21_asf/include
TGT_LIB_DIRS :=
TGT_LIB :=
LNK_SCRIPT := ./device_dep/samd21j18a_flash_0x1000.ld

TGT_INC_DIRS += /home/tng/project02/samd21_asf/include/component
TGT_INC_DIRS += /home/tng/project02/samd21_asf/include/instance
TGT_INC_DIRS += /home/tng/project02/samd21_asf/include/pio
TGT_INC_DIRS += /home/tng/project02/samd21_asf/CMSIS/Include

CFLAGS := -mthumb -Wl,-Map=$(TGT_NAME).map 
CFLAGS += -Wl,--start-group -lm -Wl,--end-group
CFLAGS += -Wl,--gc-sections -mcpu=cortex-m0plus -Wl,--entry=Reset_Handler 
CFLAGS += -Wl,--cref -T$(LNK_SCRIPT) -Wl,--section-start=.text=0x1000
CFLAGS += -D__ATSAMD21J18A__
CFLAGS += $(foreach includedir,$(TGT_INC_DIRS),-I$(includedir))

LDFLAGS += $(foreach libdir,$(TGT_LIB_DIRS),-L$(libdir))
LDFLAGS += $(foreach library,$(TGT_LIB),-l$(library))

.PHONY: all clean

%.o:	%.c
	@echo ----------------------------------------------------
	@echo Compiling $<
	arm-none-eabi-gcc -c -g -o $@ $< $(CFLAGS)
	
all: $(TGT_NAME).elf

$(TGT_NAME).elf:	$(TGT_C_OBJS) $(TGT_CXX_OBJS)
	arm-none-eabi-gcc $(CFLAGS) $(TGT_C_OBJS) $(TGT_CXX_OBJS) -o $(TGT_NAME).elf
	arm-none-eabi-objcopy -O binary $(TGT_NAME).elf $(TGT_NAME)_0x1000.bin

clean:
	@-$(RM) $(TGT_NAME).elf
	@-$(RM) $(TGT_NAME)_0x1000.bin
	@-$(RM) $(TGT_C_OBJS) $(TGT_CXX_OBJS)


