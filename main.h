/*
 * main.h
 *
 * Created: 6/8/2016 2:12:08 PM
 *  Author: 7145d33p
 */ 


#ifndef MAIN_H_
#define MAIN_H_

#define CONCAT2(x,y)            x##y
#define CONCAT3(x,y,z)          x##y##z

//LED
#define LED_PIN         30              //PB30
//SERCOM
#define UART_RX_RDY(p)          (CONCAT3(REG_SERCOM,p,_USART_INTFLAG) & SERCOM_USART_INTFLAG_RXC)
#define UART_TX_EMY(p)          (CONCAT3(REG_SERCOM,p,_USART_INTFLAG) & SERCOM_USART_INTFLAG_DRE)
#define UART_ERROR(p)           (CONCAT3(REG_SERCOM,p,_USART_STATUS & UART_ERROR_MSK)
#define UART_DATA(p)            (CONCAT3(REG_SERCOM,p,_USART_DATA))
#define EDBG_UART				3
#define EDBG_UART_TX_PIN       22      //PA22
#define EDBG_UART_RX_PIN       23      //PA23
//ADC
#define AIN4_PIN                4       //PA04
#define AIN5_PIN                5       //PA05
#define AIN6_PIN                6       //PA06
#define AIN7_PIN                7       //PA07
#define AIN8_PIN                0       //PB00
#define ADC_SET_PIN(p)          REG_ADC_INPUTCTRL = ((0xf<<24) | (0x18 << 8) | (p))     //0.5x gain
#define ADC_FLUSH()             REG_ADC_SWTRIG = 0x01         //flush old result
#define ADC_START()             REG_ADC_SWTRIG = 0x02         //start
#define ADC_RESULT_RDY()        (REG_ADC_INTFLAG & 0x01)      //result ready ?
//I2C SLAVE
#define SDAPA08_PIN				8		//PA08
#define SCLPA09_PIN				9		//PA09

//OLED1
#define DISP_RST_PIN			27		//PA27
#define DISP_DAT_CMD_PIN		30		//PB30	- collide with LED pin
#define DISP_SS_PIN				17		//PB17
#define DISP_MOSI_PIN			22		//PB22	SERCOM5_PAD2
#define DISP_SCK_PIN			23		//PB23  SERCOM5_PAD3
#define DISP_LED1_PIN			12		//PA12
#define DISP_LED2_PIN			13		//PA13
#define DISP_LED3_PIN			15		//PA15
#define DISP_BUT1_PIN			28		//PA28
#define DISP_BUT2_PIN			 2		//PA02
#define DISP_BUT3_PIN			 3		//PA03

/** Size of the receive buffer and transmit buffer. */
#define BUFFER_SIZE     (100)

/** Reference voltage for ADC,in mv. */
#define VOLT_REF        (3300)

#define CONSOLE_IRQN	95
#define ADC_IRQN		96

#define OS_NO_ERR		0

#define APP_CFG_TASK_STK_SIZE	256
#define APP_CFG_N_TASKS			10

#endif /* MAIN_H_ */
