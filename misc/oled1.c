/*
 * oled1.c
 *
 * Created: 6/8/2016 2:10:06 PM
 *  Author: 7145d33p
 */ 

#include "sam.h"
#include "main.h"
#include "oled1.h"

volatile uint8_t fbuf[4][128];

void SPI5_write_CMD(char d)
{
	OLED_CMD;
	OLED_SS_L;
	while (!(REG_SERCOM5_SPI_INTFLAG & SERCOM_SPI_INTFLAG_DRE));	//wait for empty TX BUFFER
	REG_SERCOM5_SPI_DATA = d;
	while (!(REG_SERCOM5_SPI_INTFLAG & SERCOM_SPI_INTFLAG_TXC));	//wait for empty TX BUFFER
	OLED_SS_H;
}

void SPI5_write_DATA(char d)
{
	OLED_DATA;
	OLED_SS_L;
	while (!(REG_SERCOM5_SPI_INTFLAG & SERCOM_SPI_INTFLAG_DRE));	//wait for empty TX BUFFER
	REG_SERCOM5_SPI_DATA = d;
	while (!(REG_SERCOM5_SPI_INTFLAG & SERCOM_SPI_INTFLAG_TXC));	//wait for empty TX BUFFER
	OLED_SS_H;
}

// clear everything
void oled_ClearBuffer(void) {
	memset(fbuf, 0, (128*32/8));
}

void oled_pixel(uint8_t x, uint8_t y)
{
	fbuf[y>>3][x] |= (1<<(y&0x07));
}

void oled_ShowBuf(void)
{
	uint8_t pg, col;

	//scan buffer out
	for (pg=0; pg<=3; pg++) {
		SPI5_write_CMD(0xB0 | pg);	//page 0
		SPI5_write_CMD(0x00);		//set column address 00
		SPI5_write_CMD(0x10);		//
		for (col=0; col<=127; col++) {
			SPI5_write_DATA(fbuf[pg][127-col]);	//flip left to right
		}
	}
}

void oled_Vline(uint8_t x, uint8_t y1, uint8_t y2)
{
	uint8_t ty;
	if (y1>y2) { ty=y2; y2=y1; y1=ty; }
	for (ty=y1; ty<=y2; ty++)
	oled_pixel(x,ty);
}

void oled_Hline(uint8_t x1, uint8_t x2, uint8_t y)
{
	uint8_t tx;
	if (x1>x2) { tx=x2; x2=x1; x1=tx; }
	for (tx=x1; tx<=x2; tx++)
	oled_pixel(tx, y);
}

void oled_DrawLine(uint8_t x1, uint8_t y1, uint8_t x2, uint8_t y2)
{
	uint8_t tx, ty;
	float delta, floaty;

	if (x1==x2) { oled_Vline(x1, y1, y2); return; }
	if (y1==y2) { oled_Hline(x1, x2, y1); return; }
	if (x1>x2)  { tx = x2; ty = y2; x2 = x1, y2 = y1; x1 = tx; y1 = ty; }
	delta = (float)(y2-y1)/(x2-x1);
	floaty = (float)y1;
	for (tx = x1; tx < x2; tx++) {
		oled_pixel(tx, (uint8_t)floaty);
		floaty += delta;
	}
	oled_pixel(x2, y2);
}

#if 0			//no font yet
//row   = [0..3]
//x     = [0..127]
//ascii supports only '0'-'9'
void oled_printchar(uint8_t row, uint8_t x, uint8_t ascii)
{
	uint8_t i;

	for (i=0; i<16; i++) fbuf[row][x+i] = font[ascii-'0'][i];
	for (i=0; i<16; i++) fbuf[row+1][x+i] = font[ascii-'0'][i+16];
}

void oled_print2dec(uint8_t row, uint8_t x, uint8_t num)
{
	if (num >= 100) return;	//00-99 only
	if ((num/10)==0) oled_printchar(row, x, CH_SPACE);
	else oled_printchar(row,x,(num/10)+'0');
	oled_printchar(row, x+16, (num%10)+'0');
}
#endif 

void initOLED(void)
{
	uint8_t i;
	
	OLED_SS_H;		//slave not selected
	OLED_RST_LO;	//reset
	for (i=0; i<100; i++) oled_ClearBuffer();	//some delay
	OLED_RST_HI;
	for (i=0; i<100; i++) oled_ClearBuffer();	//some delay
	//------------------------------------------------------------------
	SPI5_write_CMD(0xA8);		//set multiplex ratio
	SPI5_write_CMD(0x1F);		//
	SPI5_write_CMD(0xD3);		//set display offset
	SPI5_write_CMD(0x00);		//
	SPI5_write_CMD(0x40);		//set display start line to 00
	SPI5_write_CMD(0xC8);		//set COM output scan down
	SPI5_write_CMD(0xDA);		//set COM pins
	SPI5_write_CMD(0x02);		//
	SPI5_write_CMD(0x8F);		//set contrast
	SPI5_write_CMD(0xA4);		//entire display on
	SPI5_write_CMD(0xA6); 		//set normal, non-inverted display
	SPI5_write_CMD(0xD5);		//set display clock divider ratio
	SPI5_write_CMD(0x80);		//
	SPI5_write_CMD(0x8D);		//set charge pump
	SPI5_write_CMD(0x14);		//
	SPI5_write_CMD(0xDB);		//set VCOM deselect level
	SPI5_write_CMD(0x40);		//0x20 (0.77*VCC)
	SPI5_write_CMD(0xD9);		//set 15 clock precharge 1 clock discharge
	SPI5_write_CMD(0xF1);		//
	SPI5_write_CMD(0xAF);		//turn on display
}
