/*
 * oled1.h
 *
 * Created: 6/8/2016 2:12:28 PM
 *  Author: 7145d33p
 */ 


#ifndef OLED1_H_
#define OLED1_H_

#define	OLED_DATA			REG_PORT_OUTSET1=(1<<DISP_DAT_CMD_PIN)
#define	OLED_CMD			REG_PORT_OUTCLR1=(1<<DISP_DAT_CMD_PIN)
#define	OLED_RST_HI			REG_PORT_OUTSET0=(1<<DISP_RST_PIN)
#define	OLED_RST_LO			REG_PORT_OUTCLR0=(1<<DISP_RST_PIN)
#define	OLED_SS_H			REG_PORT_OUTSET1=(1<<DISP_SS_PIN)
#define	OLED_SS_L			REG_PORT_OUTCLR1=(1<<DISP_SS_PIN)

extern void SPI5_write_CMD(char d);
extern void SPI5_write_DATA(char d);
extern void initOLED(void);
extern void oled_ClearBuffer(void);
extern void oled_ShowBuf(void);
extern void oled_Vline(uint8_t x, uint8_t y1, uint8_t y2);
extern void oled_Hline(uint8_t x1, uint8_t x2, uint8_t y);
extern void oled_DrawLine(uint8_t x1, uint8_t y1, uint8_t x2, uint8_t y2);
extern void oled_printchar(uint8_t row, uint8_t x, uint8_t ascii);
extern void oled_print2dec(uint8_t row, uint8_t x, uint8_t num);

#endif /* OLED1_H_ */
